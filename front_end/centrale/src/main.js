import Vue from 'vue'
import App from './App.vue'
import Vuex from 'vuex'
import { Icon } from 'leaflet'
import 'leaflet/dist/leaflet.css'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// This imports all the layout components such as <b-container>, <b-row>, <b-col>:
import { LayoutPlugin } from 'bootstrap-vue'
Vue.use(LayoutPlugin)

// This imports <b-modal> as well as the v-b-modal directive as a plugin:
import { ModalPlugin } from 'bootstrap-vue'
Vue.use(ModalPlugin)

// This imports <b-card> along with all the <b-card-*> sub-components as a plugin:
import { CardPlugin } from 'bootstrap-vue'
Vue.use(CardPlugin)

// This imports directive v-b-scrollspy as a plugin:
import { VBScrollspyPlugin } from 'bootstrap-vue'
Vue.use(VBScrollspyPlugin)

// This imports the dropdown and table plugins
import { DropdownPlugin, TablePlugin } from 'bootstrap-vue'

import { ButtonPlugin } from 'bootstrap-vue'

import { FormSelectPlugin } from 'bootstrap-vue'

import { ButtonToolbarPlugin } from 'bootstrap-vue'
import { FormInputPlugin } from 'bootstrap-vue'
Vue.use(FormInputPlugin)
Vue.use(ButtonToolbarPlugin)

Vue.use(FormSelectPlugin)
Vue.use(ButtonPlugin)

Vue.use(DropdownPlugin)
Vue.use(TablePlugin)

Vue.use(Vuex)


// this part resolve an issue where the markers would not appear
delete Icon.Default.prototype._getIconUrl;

Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});


Vue.config.productionTip = false
Vue.config.devtools = true
const store = new Vuex.Store({
  state: {
    count: 0,
    liveDataInitialized: false,
    URLOfStations : [],
    data: {},
    columnRowComponents:[]
  },
  mutations: {
    increment: state => state.count++,
    decrement: state => state.count--,
    updateLiveData(state,payload){
      //state.data[payload.key] = payload.value; vue ne détecte pas la mise à jour
      Vue.set(state.data, payload.key, payload.value); //vue détecte la mise à jour
      state.liveDataInitialized = true;
      console.log(state.data);
    },
    addComponent(state,payload){
      let newRow = [];
      if(Array.isArray(state.columnRowComponents[payload.column])) {
        /*console.log("is");
        let length = state.columnRowComponents[payload.column].length;
        let start = state.columnRowComponents[payload.column].splice(0,payload.row);
        let end = state.columnRowComponents[payload.column].splice(payload.row,length-1);
        if(start.length){//vrai si longueur > 0
          newRow.push(start);
        }
        newRow.push(payload.value);
        if(end.length){
          newRow.push(end);
        }*/
        state.columnRowComponents[payload.column].splice( payload.row, 0, payload.value)
      }else{
        console.log("isnt");
        newRow = [payload.value];
        Vue.set(state.columnRowComponents, payload.column, newRow);
      }
      //Vue.set(state.columnRowComponents, payload.column, newRow);
      console.log(newRow);
    },
    logLiveData(state){
      console.log(state.data);
    },
    addStationURL(state,payload){
      state.URLOfStations.push(payload.url);
    }
  },
  actions: {
    updateLiveData (context,url) {
      console.log(context)
      console.log(url)
      fetch(url).then(result => result.json())
      .then(result =>context.commit(
        'updateLiveData',{"value":result.result,"key":url}));
    },
    logLiveData (context) {
      context.commit('logLiveData')
    },
    addComponent (context,payload) {
      context.commit('addComponent',payload)
    },
    addStationURL(context,url){
      context.commit('addStationURL',{"url":url});
      context.dispatch("updateLiveData",url);
      //on requete l'url pour
      //initialiser la donnee
    }
  }
})


new Vue({
  store: store,
  render: h => h(App),
}).$mount('#app')
