# Station Meteo Node.js

## The project

This project is a school project developped for the advanced [JavaScript course](http://ensg_dei.gitlab.io/web-az/js/exercices/projet-station-meteo/) of Master TSI option C at [Ecole Nationale des Sciences Géographiques](www.ensg.eu).

The aim was to develop a meteorogical station with two components:
1. A probe which is hosted on a Raspberry Pi (connected to the ENSG server) and composed of:
    * three sensors: thermometer, hygrometer, barometer
    * a fakesonde program simulating data of temperature, pressure, humidity, luminosity, wind, pluviometry and GNSS position
    * a Node.js server in charge of read data from the sensors (or the fakesonde program), stored them in a database and exposed them with an web API (port 80).
2. A Central Dashboard which is able to:
    * interogate APIs from diverse Raspberry Pi of the project (there are four)
    * present an historic of the probe on diverse temporality (one week, one month, one year)
    * compare data from the probes of the project
    * represent on a map the probes of the project


## Our project

We were in charge of the Raspberry Pi named _piensg009_. 

### Visualize the project

**Note:** The project can only be run on the ENSG network (because the Raspberry Pi is hosted there).

Get the project from github:
```
git clone https://gitlab.com/Syncrow/station-meteo-node.js.git
```

Run the project:
1. Go to the client folder
    ```
    cd front_end/centrale
    ```
2. Install the dependencies of the project
    ```
    npm install
    ```
3. Serve the website
    ```
    npm run serve
    ```
    Then, the website should be available at [http://localhost:8080](http://localhost:8080) or at the address given in your shell.



### Description

We have deployed a [Node.js](https://nodejs.org/en/) server on it with an [InfluxDB](https://www.influxdata.com/) database and a [Vuex](https://vuex.vuejs.org/) client.

#### The server

The code for the server is available in the folder [back_end](./back_end), with a full [README](./back_end/README.md) explaining how to deploy it on a device.

This procedure details how to deploy a server on a device of your choice which would have some sensors (a fakeSonde simulator by Cédric Esnault @cedricici is available in the [back_end/raspberryPiServices](./back_end/raspberryPiServices/) folder).

If you are on the ENSG network, no need to deploy a server. 
Indeed, our API is available on the url [http://piensg024.ensg.eu](http://piensg024.ensg.eu) and displays the data of our sensors.

The server listens on port 80 of the Raspberry Pi so it can catch all http requests for the url [http://piensg024.ensg.eu](http://piensg024.ensg.eu).
Two routes have been defined in agreement with the other groups (_cf._ [notes.md](./notes.md)):
* `/live`: which needs the parameter `capteurs` with a value corresponding to the captors wanted (_cf._ [notes.md](./notes.md) for those values)
* `/archive`: which also needs the parameter `capteurs` but also two other parameters `start` and `stop`. Their format is a date format respecting the ISO-8601 standard


#### The client

The code for the client is available in the folder [front_end/centrale](./front_end/centrale), with a [README](./front_end/centrale/README.md) explaining how to serve the website.


## Authors

 - F. Vignolles
 - V. Hue
 - H. De Paulis
