/* Imports of node modules */
var createError = require('http-errors');
var express = require('express');
var logger = require('morgan');

/* Imports of Objet InfluxDB defined as a class */
var influxDB = require('./influx')

/* Imports of files for router */
var dataRouter = require('./routes/index');

/* Time between each save into the database (in second)*/
const delay = 60*10;

/* Definition of our app */
var app = express();
app.use(logger('dev'));

/* Definition of our routes */
app.use('/', dataRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

/* Listening port 80 and starting DB work */
app.listen(80, function () {
    console.log('Example app listening on port 80!')

    influxDB.initDB();
    automaticSave();
})


// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.json({message: err.message, error: err});
});


/**
 * Run the automaticSave of captors data in the InfluxDB
 */
function automaticSave() {
    console.log("\nAutomatic Save")
    influxDB.saveSensorFile();
    influxDB.saveRainFile();
    influxDB.saveGPSFile();

    setTimeout(automaticSave, delay * 1000); 
}

module.exports = app;
