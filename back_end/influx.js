/* Imports of InfluxDB */
const Influx = require('influx');

/* Imports of GPS api */
var GPS = require('gps');
var gps = new GPS;

/* Definition of file path in the sonde device */
const pathData = "/dev/shm/";

/**
 * Creation of the class InfluxDB
 */
class InfluxDB {
    constructor() {
        this.influx = new Influx.InfluxDB({
            host: 'localhost',
            database: 'measure_data',
            schema: [
                {
                    measurement: 'sensor',
                    tags: ["_type"],
                    fields: {
                        temperature: Influx.FieldType.FLOAT,
                        pressure: Influx.FieldType.FLOAT,
                        humidity: Influx.FieldType.FLOAT,
                        luminosity: Influx.FieldType.FLOAT,
                        wind_heading: Influx.FieldType.FLOAT,
                        wind_speed_avg: Influx.FieldType.FLOAT,
                        wind_speed_max: Influx.FieldType.FLOAT,
                        wind_speed_min: Influx.FieldType.FLOAT
                    }
                },
                {
                    measurement: 'rain_counter',
                    tags: ["_type"],
                    fields: {
                        rain: Influx.FieldType.BOOLEAN
                    }
                },
                {
                    measurement: 'gps',
                    tags: ["_type"],
                    fields: {
                        longitude: Influx.FieldType.FLOAT,
                        latitude: Influx.FieldType.FLOAT
                    }
                }
            ]
        });
    }

    /**
     * Create the database measure_data if it doesn't exist
    */
    initDB() {
        console.log("\nInitialize Influx database");
        this.influx.getDatabaseNames()
            .then(names => {
                if (!names.includes('measure_data')) {
                    return this.influx.createDatabase('measure_data');
                }
            })
	    .catch( (error) => {
	    	console.log(error)
	    })
    }

    /**
     * Add the current values of the sensor file(s) into the database
    */
    saveSensorFile() {
        let fs = require('fs');
        let sensorFile = fs.readFileSync(pathData + 'sensors', 'utf8');
        let sensorData = JSON.parse(sensorFile);
        let tphFile = fs.readFileSync(pathData + 'tph.log', 'utf8');
        let tphData = JSON.parse(tphFile);

        this.influx.writePoints([
            {
                measurement: 'sensor',
                tags: { _type: "sensor data" },
                fields: {
                    temperature: tphData.temp,
                    pressure: tphData.press,
                    humidity: tphData.hygro,
                    luminosity: sensorData.measure[3].value,
                    wind_heading: sensorData.measure[4].value,
                    wind_speed_avg: sensorData.measure[5].value,
                    wind_speed_max: sensorData.measure[6].value,
                    wind_speed_min: sensorData.measure[7].value
                },
                timestamp: toTimestamp(tphData.date),
            }
        ], {
            database: 'measure_data',
            precision: 's'
        }
        ).then(() => {
            console.log("Sensor file add to the database")
        }).catch(error => {
            console.error(`Error saving data to InfluxDB! ${error.stack}`)
        });
    }

    /**
     * Add the current values of the rain file into the database
    */
    saveRainFile() {
        let fs = require('fs');
        let rainFile = fs.readFileSync(pathData + 'rainCounter.log', 'utf8');
        let rainData = rainFile.replace("\n", "");

        this.influx.writePoints([
            {
                measurement: 'rain_counter',
                tags: { _type: "rain counter" },
                fields: {
                    rain: true,
                },
                timestamp: toTimestamp(rainData)
            }
        ], {
            database: 'measure_data',
            precision: 's'
        }
        ).then(() => {
            console.log("Rain file add to the database")
        }).catch(error => {
            console.error(`Error saving data to InfluxDB! ${error.stack}`)
        });
    }

    /**
     * Add the current values of the GPS file into the database
    */
    saveGPSFile() {
        let fs = require('fs');
        let gpsFile = fs.readFileSync(pathData + 'gpsNmea', 'utf8').split("\n")[0];
        parseNmeaFile(this, gpsFile);
    }

    /**
     * Return the last value of the sensor with an asynchronous call
     * @return {Promise<JSON>} Last value of the sensor {name: String, columns: ['time', '_type', 'humidity', 'luminosity', 'pressure', 'temperature', 'wind_heading', 'wind_speed_avg', 'wind_speed_max', 'wind_speed_min'], values: [[]]}
    */
    getLastValueSensor() {
        return new Promise((resolve, reject) => {
            let fs = require('fs');
            let sensorFile = fs.readFileSync(pathData + 'sensors', 'utf8');
            let sensorData = JSON.parse(sensorFile);
            let tphFile = fs.readFileSync(pathData + 'tph.log', 'utf8');
            let tphData = JSON.parse(tphFile);
            let temperature = tphData.temp;
            let pressure = tphData.press;
            let humidity = tphData.hygro;
            let luminosity = parseFloat(sensorData.measure[3].value);
            let wind_heading = parseFloat(sensorData.measure[4].value);
            let wind_speed_avg = parseFloat(sensorData.measure[5].value);
            let wind_speed_max = parseFloat(sensorData.measure[6].value);
            let wind_speed_min = parseFloat(sensorData.measure[7].value);
            const result = { 
                name: 'sensor',
                columns: [ 'time', '_type', 'humidity', 'luminosity', 'pressure', 'temperature', 'wind_heading', 'wind_speed_avg', 'wind_speed_max', 'wind_speed_min' ],
                values: [ [ tphData.date, 'sensor data', humidity, luminosity, pressure, temperature, wind_heading, wind_speed_avg, wind_speed_max, wind_speed_min ] ] 
            };
            resolve(result);
        })

    }

    /**
     * Return the last value of the rain with an asynchronous call
     * @return {Promise<JSON>} Last value of the rain {name: string, columns: ['time', '_type', 'rain'], values: [[]]}
    */
    getLastValueRain() {
        return new Promise((resolve, reject) => {
            let fs = require('fs');
            let rainFile = fs.readFileSync(pathData + 'rainCounter.log', 'utf8');
            let rainData = rainFile.replace("\n", "");
            const result = { 
                name: 'rain_counter',
                columns: [ 'time', '_type', 'rain' ],
                values: [ [ rainData, 'rain counter', true ] ] 
            };
            resolve(result);
        });
    }

    /**
     * Return the last value of the GPS with an asynchronous call
     * @return {Promise<JSON>} Last value of the GPS {name: string, columns: ['time', '_type', 'latitude', 'longitude'], values: [[]]}
    */
    getLastValueGPS() {
        return new Promise((resolve, reject) => {
            let fs = require('fs');
            let gpsFile = fs.readFileSync(pathData + 'gpsNmea', 'utf8').split("\n")[1];
            let result;
            gps.on('data', function(gpsFileParsed) {
                result = { 
                    name: 'gps',
                    columns: [ 'time', '_type', 'latitude', 'longitude' ],
                    values: [ [ gpsFileParsed.time.toISOString(), 'gps', gpsFileParsed.lat, gpsFileParsed.lon ] ]
                };
                gps = new GPS;
                resolve(result);  
            });
            gps.update(gpsFile);   
        });
    }

    /**
     * Return the last value of the sensor between two dates with an asynchronous call
     * @param {String} start Start date (yyyy-MM-dd'T'HH:mm:ss.SSS'Z')
     * @param {String} stop Stop date (yyyy-MM-dd'T'HH:mm:ss.SSS'Z')
     * @return {Promise<JSON>} Values of the sensor between the two dates {name: string, columns: ['time', '_type', 'humidity', 'luminosity', 'pressure', 'temperature', 'wind_heading', 'wind_speed_avg', 'wind_speed_max', 'wind_speed_min'], values: [[]]}
    */
    getValuesSensorBetween(start, stop) {
        return this.influx.queryRaw(`
        select * from sensor
        where time > ` + start + ` and time < ` + stop
        )
            .then(result => getSensorData(result));
    }

    /**
     * Return the last value of the rain between two dates with an asynchronous call
     * @param {string} start Start date (yyyy-MM-dd'T'HH:mm:ss.SSS'Z')
     * @param {string} stop Stop date (yyyy-MM-dd'T'HH:mm:ss.SSS'Z')
     * @return {Promise<JSON>} Values of the rain between the two dates {name: string, columns: ['time', '_type', 'rain'], values: [[]]}
    */
    getValuesRainBetween(start, stop) {
        return this.influx.queryRaw(`
        select * from rain_counter
        where time > ` + start + ` and time < ` + stop
        )
            .then(result => getRainData(result));
    }

    /**
     * Return the last value of the GPS between two dates with an asynchronous call
     * @param {string} start Start date (yyyy-MM-dd'T'HH:mm:ss.SSS'Z')
     * @param {string} stop Stop date (yyyy-MM-dd'T'HH:mm:ss.SSS'Z')
     * @return {Promise} Values of the GPS between the two dates {name: string, columns: ['time', '_type', 'latitude', 'longitude'], values: [[]]}
    */
    getValuesGPSBetween(start, stop) {
        return this.influx.queryRaw(`
        select * from gps
        where time > ` + start + ` and time < ` + stop
        )
            .then(result => getGPSData(result));
    }
}

/**
 * Reinitialize the database
 * @param {InfluxDB} influx 
 */
function clearDatabase(influx) {
    influx.queryRaw(`
    DROP DATABASE measure_data
  `);
}

/**
 * Convert a date into the timestamp format
 * @param {string} strDate Date (yyyy-MM-dd'T'HH:mm:ss.SSS'Z')
 * @return {Number} Timestamp of the date
*/
function toTimestamp(strDate) {
    var datum = Date.parse(strDate);
    return Math.round(datum / 1000);
}

/**
 * Verify if the raw Influx output given is empty
 * @param {JSON} data Raw Influx output
 * @returns {Boolean}
 */
function isEmpty(data){
    return data.results[0].series == null;
}


/**
 * Extract sensor data from raw Influx output 
 * @param {JSON} data Raw Influx output
 * @return {JSON} Extract sensor data {name: string, columns: ['time', '_type', 'humidity', 'luminosity', 'pressure', 'temperature', 'wind_heading', 'wind_speed_avg', 'wind_speed_max', 'wind_speed_min'], values: [[]]}
*/
function getSensorData(data){
    if (isEmpty(data)){
        return { name: 'sensor', columns: ['time', '_type', 'humidity', 'luminosity', 'pressure', 'temperature', 'wind_heading', 'wind_speed_avg', 'wind_speed_max', 'wind_speed_min'], values: [] };
    }
    else{
        return data.results[0].series[0];
    }
}

/**
 * Extract rain data from raw Influx output 
 * @param {JSON} data Raw Influx output
 * @return {JSON} Extract rain data {name: string, columns: ['time', '_type', 'rain'], values: [[]]}
*/
function getRainData(data){
    if (isEmpty(data)){
        return { name: 'rain_counter', columns: ['time', '_type', 'rain'], values: []};
    }
    else{
        return data.results[0].series[0];
    }
}

/**
 * Extract GPS data from raw Influx output 
 * @param {JSON} data Raw Influx output
 * @return {JSON} Extract GPS data {name: string, columns: ['time', '_type', 'latitude', 'longitude'], values: [[]]}
*/
function getGPSData(data){
    if (isEmpty(data)){
        return { name: 'gps', columns: ['time', '_type', 'latitude', 'longitude'], values: []}
    }
    else{
        return data.results[0].series[0];
    }
}

function parseNmeaFile(influxDB, gpsFile){
    gps.on('data', function(gpsFileParsed) {
        influxDB.influx.writePoints([
            {
                measurement: 'gps',
                tags: { _type: "gps" },
                fields: {
                    longitude: gpsFileParsed.lon,
                    latitude: gpsFileParsed.lat
                },
                timestamp: Math.round(gpsFileParsed.time.getTime()/1000)
            }
        ], {
            database: 'measure_data',
            precision: 's'
        }
        ).then(() => {
            console.log("GPS file add to the database")
        }).catch(error => {
            console.error(`Error saving data to InfluxDB! ${error.stack}`)
        });
        gps = new GPS;
    });
    gps.update(gpsFile);
}


/* Instanciation of an InfluxDB Object that is exported in other module */
const influxDB = new InfluxDB();

module.exports = influxDB;
