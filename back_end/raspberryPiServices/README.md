# Fake Weather sensor

This package provide weather sensor with fake data for testing.

It was provided by Cédric Esnault (@cedricici) and somehow modified by us.

## Installation

### Prerequisites

The following softwares are needed:
* Npm 
* Node.js 
* Python 

### Run

To run the fakeSonde:
```
$ npm install
$ npm start
```

Data are saved in the folder `/dev/shm`.


### Automatize

To Automatize the fakesonde service :
1. Installe the node modules:
    ```s
    npm install
    ```
1. Add the file [fakeSonde.service](./package/fakeSonde.service) from the [package folder](./package/) in the systemD folder:
    ```s
    cd package
    cp fakeSonde.service /etc/systemd/system/fakeSonde.service
    ```
1. Start the service:
    ```s
    sudo systemctl start fakeSonde.service 
    ```
1. Make the service enabled to be started everytime the device is started:
    ```s
    sudo systemctl enable fakeSonde.service 
    ```


---

# Real SenseHat sensor

To start the real senseHat:
1. Copy the [sensehat folder](./sensehat/) in the raspberry home and rename it _senseHat_:
    ```s
    cp -r sensehat/ /home/pi/senseHat/
    ```
1. Add the file [sonde.service](./sensehat/sonde.service) from the [sensehat folder](./sensehat/) in the systemD folder:
    ```s
    cd sensehat
    cp sonde.service /etc/systemd/system/service.service
    ```
1. Start the service:
    ```s
    sudo systemctl start service.service 
    ```
1. Make the service enabled to be started everytime the device is started:
    ```s
    sudo systemctl enable service.service 
    ```

---

# Other funny services for the RaspberryPi senseHat

With the senseHat, you can do some funny things such as:
1. Print a word every minutes (see [helloWorld.py](./sensehat/helloWorld.py)).
1. Print a rainbow every minutes (see [raibow.py](./sensehat/rainbow.py)).

Those two actions can be automazid with a service such as the [helloWorldService.service](./sensehat/helloWorldService.service).