# Back-End of meteoStation

## Installation

### Server

Node.js and npm are needed to run the project. You can download them here:
[nodejs.org](https://nodejs.org)

Then, install dependencies:
```s
cd back_end
npm install
```


#### Server configuration

The server listens to the port 80 of the device. However, other services can use this port and it can be protected. In order to bypass those issues, you need to allow its use and stop all services which can be using the port 80.

One way to do this can be:
1. Stop apache2 service (if needed)
    ```s
    sudo systemctl stop apache2 
    ```
1. Make the port 80 use allowed
    ```s
    sudo setcap cap_net_bind_service=+ep /usr/bin/node
    ```

**Note:** the path `/usr/bin/node` can be different on your device. To know which path you need, run the command
```s
which node
```


### Database

Our database is managed by the [InfluxDB](https://www.influxdata.com/) tools. 

For installing it, follow those steps:
```s
sudo apt install influxdb
sudo apt install influxdb-client
```

**Note:** the path to the data file of the sensors can be modified at line 5 of the file [influx.js](./inlfux.js): `pathData`.


## Run

To start the server and the database in the same time:
```s
cd back_end
node server.js
```

**Note:** 
The first time the server is started on the device, the database is initialized but some request can be done while it is not already created. Consequently, an error can occur but only on this first time. Then, starting the server again is enough to run it whithout an other error of this type because now the database is initialized.


## Automatize the service

The deployment of the server and its database can be automatized at the start of your device. 

Indeed, the file [meteoStationServer.service](./meteoStationService.service) when its used in the systemD software suite, allows your server to be run anytime this service is enabled.

The steps to do so are:
1. Add the file in the systemD folder:
    ```s
    cp meteoStationServer.service /etc/systemd/system/meteoStationServer.service
    ```
1. Start the service:
    ```s
    sudo systemctl start meteoStationServer.service 
    ```
1. Make the service enabled to be started everytime the device is started:
    ```s
    sudo systemctl enable meteoStationServer.service 
    ```


## FakeSonde and Sonde data

Data can be collected from a real senseHat on a RaspberryPi or can be simulated with a fakeSonde server. 

Cédric Esnault (@cedricici) provides us those two services in order for us to have data for our project: [gitlab.com/cedricici/fakesonde](https://gitlab.com/cedricici/fakesonde). We have somehow modified those services, the details can be fin in the [RaspberryPiServices folder](./raspberryPiServices).



## Authors

 - F. Vignolles
 - V. Hue
 - H. De Paulis