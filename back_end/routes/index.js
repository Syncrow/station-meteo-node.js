/* Imports of express router and moment library for date*/
const moment = require('moment')
const express = require('express')
let router = express.Router();

/* Imports of Objet InfluxDB defined as a class */
let influxDB = require('../influx')

let piName = "Piensg 024";


/* Definition of route /live/ */
router.get('/live', function (req, res) {
    // Allow cross-origin from all and particulary same-origin
    res.header('Access-Control-Allow-Origin','*')

    // Get params of the query
    let captors = req.query.capteurs;


    // If the param 'capteurs' is not given or is empty
    if (!captors || !verifyCaptorsList(captors)) {
        res.json({
            error: {
                type: "request",
                message: "no or wrong parameter(s) given for capteurs, only accept : all, tem, pre, hum, lum, win, ran"
            }
        })
    }
    // If params are ok, then we get the results
    else {
        getResults(captors.split(','))
            .then((result) => {
                res.json({ result: result })
            })
            .catch((error) => {
                res.json(error)
            });
    }

})



/* Definition of route /archive/ */
router.get('/archive', function (req, res) {
    // Allow cross-origin from all and particulary same-origin
    res.header('Access-Control-Allow-Origin','*')

    // Get params of the query
    let captors = req.query.capteurs;
    let start = req.query.start;
    let stop = req.query.stop;


    // If the param 'capteurs' is not given or is empty
    if (!captors || !verifyCaptorsList(captors)) {
        res.json({
            error: {
                type: "request",
                message: "no or wrong parameter(s) given for capteurs, only accept : all, tem, pre, hum, lum, win, ran"
            }
        })
    }
    // If the params 'start' or 'stop' are not given or are not well formated (format ISO-8601)
    else if (!verifyStartStopParams(start, stop)) {
        res.json({
            error: {
                type: "request",
                message: "no or wrong parameter(s) given for start or stop, only accept date at format ISO-8601 and start need to be anterior to stop"
            }
        })
    }
    // If params are ok, then we get the results
    else {
        getResults(captors.split(','), true, start, stop)
            .then((result) => {
                res.json({ result: result })
            })
            .catch((error) => {
                res.json(error)
            });
    }

})


/* --- FUNCTIONS --- */

/**
 * Return a Promise with the json of the data from the database
 * @param {Boolean} archiveRoute 
 * @param {Date} start format ISO-8601
 * @param {Date} stop format ISO-8601
 */
function getResultsFromDB(archiveRoute = false, start = undefined, stop = undefined) {

    return new Promise((resolve, reject) => {
        // Connexion to DB and get Promise results
        let valueSensorPromise
        let valueRainPromise
        let valueGPSPromise

        // Only in archive route
        if (archiveRoute && start && stop) {
                console.log("\nfrom DB get values Archive route")
                let dateStart = (new Date(start)).getTime() * 1000000;
                let dateStop = (new Date(stop)).getTime() * 1000000;
                valueSensorPromise = influxDB.getValuesSensorBetween(dateStart, dateStop)
                valueRainPromise = influxDB.getValuesRainBetween(dateStart, dateStop)
                valueGPSPromise = influxDB.getValuesGPSBetween(dateStart, dateStop)
        }
        // Only in live route
        else {
            console.log("\nfrom DB get values Live route")
            valueSensorPromise = influxDB.getLastValueSensor()
            valueRainPromise = influxDB.getLastValueRain()
            valueGPSPromise = influxDB.getLastValueGPS()
        }

        let sensorsPromise = valueSensorPromise
            .then(organizeSensorsValues).catch(reject);
        let rainPromise = valueRainPromise
            .then(organizeRainValues).catch(reject);
        let gpsPromise = valueGPSPromise
            .then(organizeGPSValues).catch(reject);

        // We create an array with the promises in order to resolve them all at the same time
        let promises = [sensorsPromise, rainPromise, gpsPromise]

        // Managing Promises all at the sime time
        Promise.all(promises)
            .then(promises => {

                // Get the values of sensors organized in an easy way
                let sensorsValues = promises[0]
                let rainValues = promises[1]
                let gpsValues = promises[2]


                // Building of JSON result
                let result = {
                    id: 9,
                    metadata: {
                        nom: piName
                    },
                    coordinate: {
                        longitude: gpsValues.longitude,
                        latitude: gpsValues.latitude,
                        date: gpsValues.date,
                        success: true
                    },
                    measurements: {
                        temperature: {
                            description: ["Température"],
                            units: ["date", "C"],
                            data: sensorsValues.temperature
                        },
                        pressure: {
                            description: ["Pression"],
                            units: ["date", "hP"],
                            data: sensorsValues.pressure
                        },
                        humidity: {
                            description: ["Humidité"],
                            units: ["date", "%"],
                            data: sensorsValues.humidity
                        },
                        luminosity: {
                            description: ["Luminosité"],
                            units: ["date", "Lux"],
                            data: sensorsValues.luminosity
                        },
                        wind: {
                            description: ["Direction du vent", "Force moyenne du vent", "Force maxi du vent", "Force mini du vent"],
                            units: ["date", "°", "Kts", "Kts", "Kts"],
                            data: sensorsValues.wind
                        },
                        rain: {
                            description: ["Dates des basculements"],
                            units: ["date"],
                            data: rainValues
                        }
                    }
                }
                // console.log(result)
                resolve(result)
            })
            .catch(errors => {
                console.log("error in one of the getValue")
                reject({
                    error: {
                        type: "server",
                        message: "error in one of the getValue",
                        errors: {
                            sensorValue: errors[0],
                            rainValue: errors[1],
                            gpsValue: errors[2]
                        }
                    }
                })
            })
    })
}

/**
 * Return a Promise with the json of the data from the database and only the captors in listCaptors
 * @param {Array} listCaptors 
 * @param {Boolean} archiveRoute 
 * @param {Date} start format ISO-8601
 * @param {date} stop format ISO-8601
 * @returns {Promise}
 */
function getResults(listCaptors, archiveRoute = false, start = undefined, stop = undefined) {

    return new Promise((resolve, reject) => {
        // Get result from all Captors
        getResultsFromDB(archiveRoute, start, stop)
            .catch((result) => { 
                console.log(result)
                reject(result)
            })
            .then(allValues => {
                let result;

                if (listCaptors.includes("all")) {
                    result = allValues

                } else {
                    result = {
                        id: allValues.id,
                        metadata: allValues.metadata,
                        coordinate: allValues.coordinate,
                        measurements: {}
                    }
                    // Managing result to have only the captors wanted (in listCaptors)
                    listCaptors.forEach(capt => {
                        // For each captor asked, we add in result its keys and so its values from allValues
                        switch (capt) {
                            case "tem":
                                result.measurements["temperature"] = allValues.measurements.temperature;
                                break;
                            case "pre":
                                result.measurements["pressure"] = allValues.measurements.pressure;
                                break;
                            case "hum":
                                result.measurements["humidity"] = allValues.measurements.humidity;
                                break;
                            case "lum":
                                result.measurements["luminosity"] = allValues.measurements.luminosity;
                                break;
                            case "win":
                                result.measurements["wind"] = allValues.measurements.wind;
                                break;
                            case "ran":
                                result.measurements["rain"] = allValues.measurements.rain;
                                break;
                            default:
                                break;
                        }
                    })

                }
                // Return our result
                resolve(result)
            })
    })
}


/**
 * Verify if the string captors includes at list one element from our given list of captors abbrevations
 * @param {String} captors 
 * @returns True if CaptorList ok, False otherwise
 */
function verifyCaptorsList(captors) {
    let captAbbrevations = ["all", "tem", "pre", "hum", "lum", "win", "ran"]
    let oneCaptOk = false
    captAbbrevations.forEach(capt => {
        oneCaptOk = oneCaptOk || captors.includes(capt)
    });

    return oneCaptOk
}

function verifyStartStopParams(start, stop) {
    let result = moment(start, moment.ISO_8601, true).isValid()
        && moment(stop, moment.ISO_8601, true).isValid() 
        && moment(stop, moment.ISO_8601, true).isAfter(start)
    return result
}

/**
 * Reorganize the array in the format we want:
 * measurement_type: [[Date, value(s)]]
 * @param {Array} sensorsResults
 * @returns {{humidity: [[Date, Number]], luminosity: [[Date, Number]], pressure: [[Date, Number]], temperature: [[Date, Number]], wind: [[Date, Number, Number, Number, Number]]}}
 */
function organizeSensorsValues(sensorsResults) {
    let result = {
        humidity: [],
        luminosity: [],
        pressure: [],
        temperature: [],
        wind: []
    }

    sensorsResults.values.forEach(line => {
        result.humidity.push([line[0], line[2]])
        result.luminosity.push([line[0], line[3]])
        result.pressure.push([line[0], line[4]])
        result.temperature.push([line[0], line[5]])
        result.wind.push([line[0], line[6], line[7], line[8], line[9]])
    });

    // console.log(result)
    return result
}

/**
 * Reorganize the array in the format we want:
 * measurement_type: [[Date, value(s)]]
 * @param {Array} rainResults 
 * @returns {[[Date, Number]]}
 */
function organizeRainValues(rainResults) {
    let result = []

    rainResults.values.forEach(line => {
        result.push([line[0]])
    });

    // console.log(result)
    return result
}

/**
 * Prepare a gpsValue object
 * @param {Array} gpsResults 
 * @returns {{date: Date, latitude: Number, longitude: Number}}
 */
function organizeGPSValues(gpsResults) {
    let values = gpsResults.values
    let result = {
        date: null,
        latitude: null,
        longitude: null
    }

    if (gpsResults.values.length > 0) {
        result.date = values[values.length - 1][0]
        result.latitude = values[values.length - 1][2]
        result.longitude = values[values.length - 1][3]
    }
    
    // console.log(result)
    return result
}



module.exports = router;
